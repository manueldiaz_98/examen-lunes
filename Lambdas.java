package ejercicios_TC_JAVA;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.time.LocalDateTime;

public class Lambdas {

	public static void main(String[] args) {
		
		List<String> People= new ArrayList<String>();
		People.add("Manuel");
		People.add("Nadia");
		People.add("Sonia");
		Lambdas.showAllPeople(People);
		Lambdas.LambdaSupplier();

	}
	
	//Uso de expresión lambda
	public static void showAllPeople (List<String> People) {

		People.forEach(Person->System.out.println(Person));
		
	}
	
	//Expresión lambda supplier
	public static void LambdaSupplier() {
		Supplier <LocalDateTime> s=() -> LocalDateTime.now();
		LocalDateTime time=s.get();
		System.out.println("La fecha actual es: "+time);
	}

}
