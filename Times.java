package ejercicios_TC_JAVA;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Times {

	public static void main(String[] args) {
		
		obtenerFecha();
		obtenerFechaCompleta();

	}
	
	public static void obtenerFecha () {
		LocalDate fecha = LocalDate.now();
		System.out.println("La fecha de hoy es: "+fecha);
	}
	
	public static void obtenerFechaCompleta() {
		LocalDateTime fecha=LocalDateTime.now();
		System.out.println("Fecha: "+fecha+" Hora: "+fecha.getHour()+":"+fecha.getMinute());
	}

}
