package ejercicios_TC_JAVA;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Streams {

	public static void main(String[] args) {
		
		List<String> People= new ArrayList<String>();
		People.add("Manuel");
		People.add("Nadia");
		People.add("Sonia");
		Streams.contarElementos(People);
		Streams.filtrarElementos(People);

	}
	
	
	//Contar elementos de un stream
	public static void contarElementos(List<String> People) {
		
		Stream <String> peopleStream = People.stream();
		System.out.println("El stream contiene "+peopleStream.count()+" elementos");
		
	}
	
	//Utilizando stream, filter y foreach
	
	public static void filtrarElementos(List<String> People) {
		Stream <String> peopleStream = People.stream();
		System.out.println("Los elementos que contienen la letra i son: ");
		peopleStream.filter(person->person.contains("i"))
		.forEach(person->System.out.println(person));
	}

}
