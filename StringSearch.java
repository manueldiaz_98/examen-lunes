package ejercicios_TC_JAVA;
import java.util.Optional;

public class StringSearch {
	public String search(Integer id) {
		
		switch(id) {
		 
		case 1 : return new String ("Perro");
		case 2 : return new String ("Gato");
		case 3 : return new String ("Oso");
		default: return null;
		
		}
	}
	
	public Optional<String>searchOptional(Integer id) {
		
		switch(id) {
		 
		case 1 : return Optional.of(new String("Perro"));
		case 2 : return Optional.of(new String("Gato"));
		case 3 : return Optional.of(new String("Oso"));
		default: return Optional.empty();
		
		}
	}
}