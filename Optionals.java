package ejercicios_TC_JAVA;
import java.util.Optional;

public class Optionals {
	
	public static StringSearch stringSearch;

	public static void main(String[] args) {
		
		stringSearch=new StringSearch();
		searchStringUsingOptional();
		searchStringUsingOptional2();

	}
	
	public static void searchStringUsingOptional() {
		
		Optional<String> optionalString=stringSearch.searchOptional(1);
		if (optionalString.isPresent()) {
			System.out.println("El animal existe");
		}
		
	}
	
	public static void searchStringUsingOptional2() {
		
		Optional<String> optionalString=stringSearch.searchOptional(4);
		if (optionalString.isEmpty()) {
			System.out.println("El animal no existe");
		}
		
	}

}
